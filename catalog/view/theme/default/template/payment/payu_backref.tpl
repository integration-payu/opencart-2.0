<?php echo $header; ?>
<div class="container">
    <h2>Спасибо за ваш заказ</h2>
    <div class="center">
        <?php if(!isset($result)) { ?>
            Невозможно узнать статус заказа.
        <?php } elseif($result == 1) { ?>
            Ошибка обработки платежа.
        <?php } elseif($result == -1) { ?>
            Счет выставлен. Ожидается оплата.
        <?php } elseif($result == 0) { ?>
            Заказ успешно принят.
        <?php } ?>
        <a href="<?php echo $home; ?>">Вернуться на главную</a>
    </div>
</div>
<?php echo $footer; ?>