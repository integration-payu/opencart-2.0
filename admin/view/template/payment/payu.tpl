<?php echo $header; ?>
<style>
    .box {
        border: 1px solid #E5E5E5;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
        margin: 20px;
    }
    .heading:after {
        content: ".";
        display: block;
        height: 0;
        clear: both;
        visibility: hidden;
    }
    .heading {
        border-bottom: 1px solid #E5E5E5;
        background-color: #EDEDED;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }
    .buttons {
        float: right;
        padding-top: 4px;
        margin-right: 8px;
    }
    .logo {
        float: left;
        margin-left: 8px;
    }
    .content {
        margin: 20px 10px 10px 10px;
    }
    .required {
        color: #FF0000;
    }
    .form tr {
        border-bottom: 1px dotted #E5E5E5;
    }
    .label-form {
        padding: 5px;
        width: 25%;
        float: left;
        display: table-cell;
    }
    .input {
        padding: 5px;
        width: 15%;
        float: left;
    }
    .form {
        width: 100%;
    }
    .form-control {

    }
</style>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-free-checkout" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1 class="logo"><img src="view/image/payment/payu.png" style="height:25px; margin-top:-5px;" /></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="btn btn-primary"><?php echo $button_save; ?></a>&nbsp;<a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-danger"><?php echo $button_cancel; ?></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form form-group">
                    <tr>
                        <td class="label-form"><span class="required">*</span> <?php echo $entry_merchant; ?></td>
                        <td class="input"><input type="text" name="payu_merchant" class="form-control" value="<?php echo $payu_merchant; ?>" />
                            <?php if ($error_merchant) { ?>
                            <span class="error"><?php echo $error_merchant; ?></span>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td class="label-form"><span class="required">*</span> <?php echo $entry_secretkey; ?></td>
                        <td class="input"><input type="text" name="payu_secretkey" class="form-control" value="<?php echo $payu_secretkey; ?>" />
                            <?php if ($error_secretkey) { ?>
                            <span class="error"><?php echo $error_secretkey; ?></span>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td class="label-form"><?php echo $entry_debug; ?></td>
                        <td class="input"><select name="payu_debug" class="form-control">
                                <? $st0 = $st1 = "";
                 if ( $payu_debug == 0 ) $st0 = 'selected="selected"';
                  else $st1 = 'selected="selected"';
              ?>
                                <option value="1" <?= $st1 ?> ><?php echo $entry_debug_on; ?></option>
                                <option value="0" <?= $st0 ?> ><?php echo $entry_debug_off; ?></option>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="label-form"><?php echo $entry_LU; ?></td>
                        <td class="input"><input type="text" name="payu_LU" value="<?php echo $payu_LU; ?>" class="form-control" /></td>
                    </tr>

                    <tr>
                        <td class="label-form"><?php echo $entry_currency; ?></td>
                        <td class="input"><input class="form-control" type="text" name="payu_currency" value="<?php echo ($payu_currency == "") ? "RUB" : $payu_currency; ?>" /></td>
                    </tr>

                    <tr>
                        <td class="label-form"><?php echo $entry_backref; ?></td>
                        <td class="input"><input class="form-control" type="text" name="payu_backref" value="<?php echo $payu_backref; ?>" /></td>
                    </tr>


                    <tr>
                        <td class="label-form"><?php echo $entry_vat; ?></td>
                        <td class="input"><input class="form-control" type="text" name="payu_vat" value="<?php echo ($payu_vat == "") ? 0 : $payu_vat; ?>" /></td>
                    </tr>

                    <tr>
                        <td class="label-form"><?php echo $entry_order_type; ?></td>
                        <td class="input"><select class="form-control" name="payu_entry_order_type">
                                <? $st0 = $st1 = "";
                 if ( $payu_entry_order_type == 0 ) $st0 = 'selected="selected"';
                  else $st1 = 'selected="selected"';
                ?>

                                <option value="0" <?= $st0 ?> ><?php echo $entry_order_net; ?></option>
                                <option value="1" <?= $st1 ?> ><?php echo $entry_order_gross; ?></option>
                            </select></td>
                    </tr>

                    <tr>
                        <td class="label-form"><?php echo $entry_language; ?></td>
                        <td class="input"><input class="form-control" type="text" name="payu_language" value="<?php echo ($payu_language == "") ? "RU" : $payu_language; ?>" /></td>
                    </tr>

                    <tr>
                        <td class="label-form"><?php echo $entry_order_status; ?></td>
                        <td class="input"><select class="form-control" name="payu_order_status_id">
                                <?php
                foreach ($order_statuses as $order_status) { 

                $st = ($order_status['order_status_id'] == $payu_order_status_id) ? ' selected="selected" ' : ""; 
                ?>
                                <option value="<?php echo $order_status['order_status_id']; ?>" <?= $st ?> ><?php echo $order_status['name']; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="label-form"><?php echo $entry_status; ?></td>
                        <td class="input"><select class="form-control" name="payu_status">
                                <? $st0 = $st1 = "";
                 if ( $payu_status == 0 ) $st0 = 'selected="selected"';
                  else $st1 = 'selected="selected"';
                ?>

                                <option value="1" <?= $st1 ?> ><?php echo $text_enabled; ?></option>
                                <option value="0" <?= $st0 ?> ><?php echo $text_disabled; ?></option>

                            </select></td>
                    </tr>
                    <tr>
                        <td class="label-form"><?php echo $entry_sort_order; ?></td>
                        <td class="input"><input class="form-control" type="text" name="payu_sort_order" value="<?php echo $payu_sort_order; ?>" size="1" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>